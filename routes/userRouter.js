const express = require('express');
const {getOne} = require('../controllers/userController.js');
const userRouter = express.Router();

userRouter.get('/me', getOne);

module.exports = userRouter;
