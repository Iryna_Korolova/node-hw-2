const User = require('../models/user');
const jwt = require('jsonwebtoken');
require('dotenv').config();

exports.auth = function(req, res, next) {
  if (req.headers.authorization) {
    jwt.verify(req.headers.authorization.split(' ')[1],
        process.env.TOKEN_KEY, async (err, payload) => {
          if (payload) {
            const user = await User.findOne({_id: payload.id});
            if (user) {
              req.userId = user._id;
              next();
            } else {
              return res.status(400).send({message: 'Unauthorized'});
            }
          } else {
            return res.status(400).send({message: 'Unauthorized'});
          }
        });
  } else {
    return res.status(400).send({message: 'Unauthorized'});
  }
};
