const User = require('../models/user');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
require('dotenv').config();


exports.register = async function(req, res, next) {
  const {username, password} = req.body;
  if (!username || !password) {
    res.status(400).send({
      message: 'Not found username or password',
    });
    return;
  }
  const existUser = await User.findOne({username});
  if (existUser) {
    res.status(400).send({
      message: 'A user with the same username already exist',
    });
    return;
  }
  try {
    const passwordHash = await bcrypt.hash(password, 2);
    const newUser = new User({username, password: passwordHash});
    await newUser.save();
    res.send({
      message: 'Success',
    });
  } catch (error) {
    res.status(500).send(error);
  }
};
exports.login = async function(req, res, next) {
  const {username, password} = req.body;
  if (!username || !password) {
    res.status(400).send({
      message: 'Not found username or password',
    });
    return;
  }
  const user = await User.findOne({username});
  if (!user) {
    return res.status(400).send({
      message: 'Not found',
    });
  }
  const correct = await bcrypt.compare(password, user.password);
  if (!correct) {
    return res.status(400).send({
      message: 'Wrong password',
    });
  }
  return res.status(200).send({
    message: 'Success',
    jwt_token: jwt.sign({id: user._id, username: user.username},
        process.env.TOKEN_KEY),
  });
};
