require('dotenv').config();
const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const userRouter = require('./routes/userRouter.js');
const noteRouter = require('./routes/noteRouter.js');
const authRouter = require('./routes/authRouter.js');
const {auth} = require('./middlewares/auth.js');
const fs = require('fs');


const app = express();
const PORT = 8080;

app.use(express.json());
app.use(cors());
app.use((req, res, next) => {
  const log = `${new Date().toLocaleString()} ${req.method} ${req.originalUrl}`;
  fs.appendFile('logs.log', log + '\n', function(error) {
    if (error) {
      return;
    }
  });
  next();
});

mongoose.connect(`mongodb+srv://${process.env.DB_LOGIN}:${process.env.DB_PASSWORD}@cluster0.rhkmc.mongodb.net/myFirstDatabase?retryWrites=true&w=majority`);

app.use('/api/users', [auth, userRouter]);
app.use('/api/notes', [auth, noteRouter]);
app.use('/api/auth', authRouter);

app.listen(PORT, () => {
  console.log(`Example app listening on port ${PORT}`);
});
